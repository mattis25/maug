EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:tft_14pin
LIBS:fcp-14_0.8mm-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L tft_display U1
U 1 1 5A36464F
P 4550 1800
F 0 "U1" H 4850 1900 60  0000 C CNN
F 1 "tft_display" H 4350 1850 60  0000 C CNN
F 2 "tft:tft_14" H 4550 1800 60  0001 C CNN
F 3 "" H 4550 1800 60  0001 C CNN
	1    4550 1800
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X14 P1
U 1 1 5A364691
P 4800 3050
F 0 "P1" H 4800 3800 50  0000 C CNN
F 1 "CONN_01X14" V 4900 3050 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x14_Pitch2.54mm" H 4800 3050 50  0001 C CNN
F 3 "" H 4800 3050 50  0000 C CNN
	1    4800 3050
	0    -1   1    0   
$EndComp
Wire Wire Line
	5450 2300 5450 2850
Wire Wire Line
	5350 2850 5350 2300
Wire Wire Line
	5250 2300 5250 2850
Wire Wire Line
	5150 2850 5150 2300
Wire Wire Line
	5050 2300 5050 2850
Wire Wire Line
	4950 2850 4950 2300
Wire Wire Line
	4850 2300 4850 2850
Wire Wire Line
	4750 2850 4750 2300
Wire Wire Line
	4650 2300 4650 2850
Wire Wire Line
	4550 2850 4550 2300
Wire Wire Line
	4450 2300 4450 2850
Wire Wire Line
	4350 2850 4350 2300
Wire Wire Line
	4250 2300 4250 2850
Wire Wire Line
	4150 2850 4150 2300
$EndSCHEMATC
