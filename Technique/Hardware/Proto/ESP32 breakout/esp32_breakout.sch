EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ESP32-footprints-Shem-Lib
LIBS:esp32_breakout-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ESP32-WROOM U1
U 1 1 5A3679C3
P 5350 3950
F 0 "U1" H 4650 5200 60  0000 C CNN
F 1 "ESP32-WROOM" H 5850 5200 60  0000 C CNN
F 2 "ESP32-footprints-Lib:ESP32-WROOM" H 5700 5300 60  0001 C CNN
F 3 "" H 4900 4400 60  0001 C CNN
	1    5350 3950
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X14 P1
U 1 1 5A367A10
P 3850 3900
F 0 "P1" H 3850 4650 50  0000 C CNN
F 1 "CONN_01X14" V 3950 3900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x14_Pitch2.54mm" H 3850 3900 50  0001 C CNN
F 3 "" H 3850 3900 50  0000 C CNN
	1    3850 3900
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X15 P3
U 1 1 5A367B6B
P 6550 3700
F 0 "P3" H 6550 4500 50  0000 C CNN
F 1 "CONN_01X15" V 6650 3700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x15_Pitch2.54mm" H 6550 3700 50  0001 C CNN
F 3 "" H 6550 3700 50  0000 C CNN
	1    6550 3700
	1    0    0    1   
$EndComp
$Comp
L CONN_01X10 P2
U 1 1 5A367BB3
P 5350 5200
F 0 "P2" H 5350 5750 50  0000 C CNN
F 1 "CONN_01X10" V 5450 5200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x10_Pitch2.54mm" H 5350 5200 50  0001 C CNN
F 3 "" H 5350 5200 50  0000 C CNN
	1    5350 5200
	0    -1   1    0   
$EndComp
Wire Wire Line
	4050 3350 4400 3350
Wire Wire Line
	4050 3250 4050 2950
Wire Wire Line
	4050 2950 3550 2950
Wire Wire Line
	3550 2950 3550 4900
Wire Wire Line
	3550 4900 4400 4900
Wire Wire Line
	4400 4900 4400 4650
Wire Wire Line
	4050 3450 4400 3450
Wire Wire Line
	4400 3550 4050 3550
Wire Wire Line
	4050 3650 4400 3650
Wire Wire Line
	4400 3750 4050 3750
Wire Wire Line
	4050 3850 4400 3850
Wire Wire Line
	4400 3950 4050 3950
Wire Wire Line
	4050 4050 4400 4050
Wire Wire Line
	4400 4150 4050 4150
Wire Wire Line
	4050 4250 4400 4250
Wire Wire Line
	4400 4350 4050 4350
Wire Wire Line
	4050 4450 4400 4450
Wire Wire Line
	4400 4550 4050 4550
Wire Wire Line
	6250 4400 6350 4400
Wire Wire Line
	6350 4300 6250 4300
Wire Wire Line
	6250 4200 6350 4200
Wire Wire Line
	6350 4100 6250 4100
Wire Wire Line
	6250 4000 6350 4000
Wire Wire Line
	6350 3900 6250 3900
Wire Wire Line
	6250 3800 6350 3800
Wire Wire Line
	6350 3700 6250 3700
Wire Wire Line
	6250 3600 6350 3600
Wire Wire Line
	6350 3500 6250 3500
Wire Wire Line
	6250 3400 6350 3400
Wire Wire Line
	6350 3300 6250 3300
Wire Wire Line
	6250 3200 6350 3200
Wire Wire Line
	6250 4600 6850 4600
Wire Wire Line
	6850 4600 6850 2750
Wire Wire Line
	6850 2750 6200 2750
Wire Wire Line
	6200 2750 6200 3100
Wire Wire Line
	6200 3100 6350 3100
Wire Wire Line
	6250 4500 6750 4500
Wire Wire Line
	6750 4500 6750 2850
Wire Wire Line
	6750 2850 6350 2850
Wire Wire Line
	6350 2850 6350 3000
$EndSCHEMATC
