#include "WiFi.h"                

#include "SPI.h"
#include "Adafruit_GFX.h"
#include "Adafruit_ILI9341.h"
#define TFT_DC 17
#define TFT_CS 5
#define TFT_MOSI 23
#define TFT_CLK 18
#define TFT_RST 16
#define TFT_MISO 19
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC, TFT_MOSI, TFT_CLK, TFT_RST, TFT_MISO);

int threshold = 30;
bool M = false;
bool A = false;
bool U = false;
bool G = false;
void gotTouchM(){ M = true; }
void gotTouchA(){ A = true; }
//void gotTouchU(){ U = true; }
void gotTouchG(){ G = true; }


void setup() {

  touchAttachInterrupt(T7, gotTouchM, threshold);
  touchAttachInterrupt(T6, gotTouchA, threshold);
//  touchAttachInterrupt(T4, gotTouchU, threshold);
  touchAttachInterrupt(T5, gotTouchG, threshold);
  
  pinMode(22, OUTPUT);
  digitalWrite(22, HIGH);

  Serial.begin(115200);

 splashscreen();

 WiFi.mode(WIFI_STA);
 WiFi.disconnect();
 delay(100);

pinMode(32, INPUT); 
//streak();

}

void loop() {
   /* int M = touchRead(T7);
    int A = touchRead(T6);
    int U = touchRead(T4);
    int G = touchRead(T5); */

delay(600);
    int ifTop = digitalRead(32);
   Serial.print(M);  // DEBUG 
   Serial.print("  ");
   Serial.println(A);
  
    streak();
    wifiScan();


  /*else if (U){
   tft.fillScreen(ILI9341_WHITE);
   tft.setCursor(30, 100);
   tft.setTextSize(4); tft.setTextColor(ILI9341_PINK);
   tft.println("Coucou :*");
   }*/
      
      

}

void splashscreen(){
   tft.begin();
   tft.setRotation(3);
   tft.fillScreen(ILI9341_WHITE);
   tft.setCursor(92, 90);
   tft.setTextColor(ILI9341_BLACK); tft.setTextSize(5);
   tft.println("Maug");
   delay(1200);

   tft.setCursor(50, 200);
   tft.setTextSize(2);
   tft.println("Appuyez sur M pour");
   tft.setCursor(90, 220);
   tft.print("continuer");
  }

void streak(){
  if(A){
    tft.fillScreen(ILI9341_WHITE);
    tft.setCursor(30, 100);
    tft.setTextSize(4); tft.setTextColor(ILI9341_RED);
    tft.println("Flammes"); }
  }

void wifiScan(){

  if(M){
   tft.fillScreen(ILI9341_WHITE);
    tft.setCursor(25, 100);
    tft.setTextSize(2); tft.setTextColor(ILI9341_BLACK);
    tft.println("Chargement en cours"); 
    

    tft.setCursor(10, 10);
    tft.setTextSize(2); tft.setTextColor(ILI9341_BLACK);

    int n = WiFi.scanNetworks();
  
    tft.fillScreen(ILI9341_WHITE);
    tft.setCursor(5, 5);
    
    if (n == 0) {
      tft.println("Aucun reseau trouvé");
    }
    
    else {
      tft.print(n);
      tft.println(" reseaux wifi detectes:");
      
      for (int i = 0; i < n; ++i) {
        tft.setTextSize(2); tft.setCursor(10, (i*20 + 40)); 
        //.remove(20)
        String wifiSSID = WiFi.SSID(i);
        wifiSSID.remove(22);  //Limite la longueur du nom à 22 caractères
        tft.print(wifiSSID);
        delay(10);
       }
    }
    
    tft.println("");
    delay(60000);

}
  
  }
