#include "SPI.h"
#include "Adafruit_GFX.h"
#include "Adafruit_ILI9341.h"

// Ecran
#define TFT_DC 17
#define TFT_CS 5
#define TFT_MOSI 23
#define TFT_CLK 18
#define TFT_RST 16
#define TFT_MISO 19
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC, TFT_MOSI, TFT_CLK, TFT_RST, TFT_MISO);

unsigned long time_since_function;
unsigned long time_begin_function;

// Boutons tactiles
int threshold = 30;
bool M = false;
bool A = false;
bool U = false;
bool G = false;
bool UP = false;
bool DOWN = false;
bool L = false;
bool R = false;
void gotTouchM(){ M = true; }
void gotTouchA(){ A = true; }
void gotTouchU(){ U = true; }
void gotTouchG(){ G = true; }
void gotTouchUP(){ UP = true; }
void gotTouchDOWN(){ DOWN = true; }
void gotTouchL(){ L = true; }
void gotTouchR(){ R = true; }

char battery_level_pin = 35;
int battery_level; //Battery level, ADC 12 bits

char blue_led = 26;
char white_led = 32;
char red_led = 33;
char motor_pin = 21;
char push_pin = 39;
char spst_pin = 36;

char backlight_pin = 22; // Not working, le retroéclairage ne peut pas être controllé


// GAME'S VAR
int score = 0;
int first_game = 1;
float x;
float y;
bool hg;
bool hd;
bool bg;
bool bd;
bool zonehg;
bool zonehd;
bool zonebg;
bool zonebd;
bool firstpassage;
int gravite = 10;
int poids = 1;
bool plateforme = false;
bool gravity = true;
int a = 5;
float vy = 0;
float vx = 0;
int time_begin_game;
int tempsjeu = 40000;
int pointvalidation = 0;
bool firstpassagehg = false;
bool firstpassagehd = false;
bool firstpassagebg = false;
bool firstpassagebd = false;
bool victoire = false;
int time_sec_begin = millis();
bool death = false;

void setup() {

  Serial.begin(115200);

  ecran_acceuil(); 

  pinMode(backlight_pin, OUTPUT);  //Retroéclairage écran
  digitalWrite(backlight_pin, HIGH);

  pinMode(battery_level_pin, INPUT);
  pinMode(push_pin, INPUT);
  pinMode(spst_pin, INPUT);
  pinMode(blue_led, OUTPUT);
  pinMode(white_led, OUTPUT);
  pinMode(red_led, OUTPUT);
  pinMode(motor_pin, OUTPUT);
  
  touchAttachInterrupt(T7, gotTouchM, threshold);
  touchAttachInterrupt(T6, gotTouchA, threshold);
  touchAttachInterrupt(T4, gotTouchU, threshold);
  touchAttachInterrupt(T5, gotTouchG, threshold);
  touchAttachInterrupt(T0, gotTouchUP, threshold);
  touchAttachInterrupt(T3, gotTouchDOWN, threshold);
  touchAttachInterrupt(T2, gotTouchL, threshold);
  touchAttachInterrupt(T1, gotTouchR, threshold);
 
  delay(1000); // Pour laisser le temps d'executer le setup
  
}

int page_en_cours = 0;

void loop() {
  switch (page_en_cours) {
    case 0:
      if (U) {
        passage_ecran1();
        page_en_cours = 1;
      }
      break;
    case 1:
      if (A) {
        init_ecran_jeu();
        page_en_cours = 2;
      }
      break;
    case 2:
      jeu();  
      break;
    case 3:
      if (A){
      page_en_cours = 1;
      A = 0;
      }  
      break;
  }
}

void ecran_acceuil(){
  tft.begin();
  tft.setRotation(3);
  delay(1000);
   
  tft.fillScreen(ILI9341_PINK);
  tft.setCursor(92, 30);
  tft.setTextColor(ILI9341_BLUE); 
  tft.setTextSize(5);
  tft.println("Maug");
  tft.setCursor(70,90);
  tft.setTextSize(2);
  tft.println("Bienvenue !");
  tft.setCursor(30, 130);
  tft.setTextSize(2);
  tft.println("Paris Maker Faire 2018");
  delay(1200);
  tft.setCursor(50, 200);
  tft.setTextSize(2);
  tft.println("Appuyez sur U pour");
  tft.setCursor(140, 220);
  tft.print("jouer");  
}

void partiehg(){
    tft.fillScreen(ILI9341_WHITE);
    //On construit les blocs de sortie
    tft.fillRect(310, 75, 10, 100, ILI9341_GREEN);
    tft.fillRect(100,230,100,10, ILI9341_GREEN);
    //On construit les plateformes
    tft.fillRect(110,50,100,5, ILI9341_ORANGE);
    tft.fillRect(50,100,50,5, ILI9341_ORANGE);
    tft.fillRect(220,100,50,5, ILI9341_ORANGE);
    tft.fillRect(110,150,100,5, ILI9341_ORANGE);
    tft.fillRect(50,200,50,5, ILI9341_ORANGE);
    tft.fillRect(220,200,50,5, ILI9341_ORANGE);
    //points de validation de biome
    if(firstpassagehg == false){
      tft.fillRect(240,190,10,10, ILI9341_BLACK);
    }
    if (pointvalidation  == 4){
      tft.fillRect(50,50,50,50,ILI9341_GREEN);
    }
    //zones mortelles
    tft.fillRect(0,237,100,3, ILI9341_RED);
    tft.fillRect(200,237,120,3, ILI9341_RED);
    //tft.fillRect(250,197,20,3, ILI9341_RED);
  }

  void partiehd(){
    tft.fillScreen(ILI9341_WHITE);
    //On construit les blocs de sortie
    tft.fillRect(0, 75, 10, 100, ILI9341_PINK); 
    tft.fillRect(100,230,100,10, ILI9341_PINK);
    //On construit les plateformes
    tft.fillRect(110,50,100,5, ILI9341_BLACK);
    tft.fillRect(50,100,50,5, ILI9341_BLACK); 
    tft.fillRect(220,100,50,5, ILI9341_BLACK);
    tft.fillRect(110,150,100,5, ILI9341_BLACK);
    tft.fillRect(50,200,50,5, ILI9341_BLACK);
    tft.fillRect(220,200,50,5, ILI9341_BLACK);
    //points de validation de biome
    if(firstpassagehd == false){
      tft.fillRect(70,190,10,10, ILI9341_BLACK);
    }
    //zones mortelles
    tft.fillRect(0,237,100,3, ILI9341_RED);
    tft.fillRect(200,237,120,3, ILI9341_RED);
    //tft.fillRect(50,197,20,3, ILI9341_RED);
  }

  void partiebg(){
    tft.fillScreen(ILI9341_WHITE);
    //On construit les blocs de sortie    
    tft.fillRect(310, 75, 10, 100, ILI9341_BLACK); // rectangle droit
    tft.fillRect(100,0,100,10, ILI9341_BLACK); // rectangle haut
    //On construit les plateformes
    tft.fillRect(110,50,100,5, ILI9341_BLUE);
    tft.fillRect(50,100,50,5, ILI9341_BLUE);
    tft.fillRect(220,100,50,5, ILI9341_BLUE);
    tft.fillRect(110,150,100,5, ILI9341_BLUE);
    tft.fillRect(50,200,50,5, ILI9341_BLUE);
    tft.fillRect(220,200,50,5, ILI9341_BLUE);
    //points de validation de biome
    if(firstpassagebg == false){
      tft.fillRect(240,90,10,10, ILI9341_BLACK);
    }
    //zones mortelles
    tft.fillRect(0,237,320,3, ILI9341_RED);
    //tft.fillRect(250,97,20,3, ILI9341_RED);
  }
 

  void partiebd(){
    tft.fillScreen(ILI9341_WHITE);
    //On construit les blocs de sortie
    tft.fillRect(0, 75, 10, 100, ILI9341_PURPLE); // rectangle gauche
    tft.fillRect(100,0,100,10, ILI9341_PURPLE); // rectangle haut
   //On construit les plateformes
    tft.fillRect(110,50,100,5, ILI9341_CYAN);
    tft.fillRect(50,100,50,5, ILI9341_CYAN);
    tft.fillRect(220,100,50,5, ILI9341_CYAN);
    tft.fillRect(110,150,100,5, ILI9341_CYAN);
    tft.fillRect(50,200,50,5, ILI9341_CYAN);
    tft.fillRect(220,200,50,5, ILI9341_CYAN);
    //points de validation de biome
    if(firstpassagebd == false){
      tft.fillRect(70,90,10,10, ILI9341_BLACK);
    }
    //zones mortelles
    tft.fillRect(0,237,320,3, ILI9341_RED);
    //tft.fillRect(50,97,20,3, ILI9341_RED);
  }

void deplacement(){
      if(plateforme == true){
        gravity = false;
        vy = 0;
      }

      if(plateforme == false){
        gravity = true;
      }

      if(M && plateforme == true){
        vy = -25;
        plateforme = false;
        gravity = true;
      }

      
        if(G && x > 0){
          x = x - 5;
        }
  
        if(A && x < 306){
          x = x + 5;
        }

      // fonction qui permet de gérer la gravité
      if(gravity == true){
        if (y > 0){
          if(y < 230){
            vy = vy + a * 0.05;
            y = y + vy * 0.05;
          }else{
            y = y;
          }
        } else if (y <= 0){
          vy = 0;
          y = 0.1;
        }
      }
      
      M = false;
      A = false;
      U = false;
      G = false;

      bord_plateformes();
      
      //on dessine le carré dans sa nouvelle position
      tft.fillRect(x, y, 10, 10, ILI9341_BLUE);
  }

void statut() {
  if (pointvalidation == 4 && (millis() - time_begin_game) < tempsjeu && hg == true && (x > 50 && x < 100 && y > 50 && y < 100)){
          victoire = true; 
      }

      if (victoire == false){
          if((millis() - time_begin_game) > tempsjeu){ //Limite de temps (40 secondes pour le jeu démo)
            page_en_cours = 3;
            tft.fillScreen(ILI9341_WHITE);
            tft.setTextSize(4);
            tft.setCursor(100, 85);
            tft.setTextColor(ILI9341_BLACK);
            tft.print("Temps");
            tft.setCursor(80, 130);
            tft.print("ecoule !");
            delay(1000);
            losing();
         }
      }

      if(victoire == true){
        winning();
      }

     if (death == true){
      losing();
     }
}

void losing(){
  digitalWrite(motor_pin, HIGH);
  digitalWrite(red_led, HIGH);
  tft.fillScreen(ILI9341_RED);
  tft.setCursor(40, 90);
  tft.setTextSize(4);
  tft.setTextColor(ILI9341_WHITE);
  tft.print("Perdu !");
  tft.setCursor(75, 160);
  delay(1000);
  digitalWrite(motor_pin, LOW);
  tft.setTextSize(2);
  tft.print("Appuyez sur A");
  tft.setCursor(72, 185);
  tft.print("pour reessayer");
  page_en_cours = 3;
}

void winning(){
  digitalWrite(motor_pin, HIGH);
  tft.fillScreen(ILI9341_GREEN);
  tft.setCursor(20, 90);
  tft.setTextSize(4);
  tft.setTextColor(ILI9341_WHITE);
  tft.print("Bien joue !");
  tft.setCursor(75, 160);
  delay(1000);
  digitalWrite(motor_pin, LOW);
  tft.setTextSize(2);
  tft.print("Appuyez sur A");
  tft.setCursor(72, 185);
  tft.print("pour reessayer");
  first_game++;
  page_en_cours = 3; 
}

void passage_ecran1 () {
  if(first_game == 1){  // Les règles sont affichées à chaque démarrage de la console
      U = false;
      tft.fillScreen(ILI9341_WHITE);
      tft.setCursor(50,10);
      tft.print("Regles du jeu");
      tft.setCursor(10,50);
      tft.setTextSize(2);
      tft.print("G/A pour se deplacer");
      tft.setCursor(10,75);
      tft.print("a gauche/droite");
      tft.setCursor(10,100);
      tft.print("M pour sauter");
      tft.setCursor(10, 125);
      tft.print("but : -4 checkpoint");
      tft.setCursor(80,150);
      tft.print("-Retourner au depart");
      tft.setCursor(10,200);
      delay(1000);
      A = 0;
      tft.print("A pour commencer a jouer");
      first_game = 0;       
    }
}
    
void init_ecran_jeu () {
    // On redonne les valeurs par défaut aux variables
      digitalWrite(blue_led, LOW);
      digitalWrite(red_led, LOW);     
      A = 0;
    //zones de transition de biome
      zonehd = false;
      zonehg = false;
      zonebg = false;
      zonebd = false;
    //variables de chaque biome
      hg = true;
      hd = false;
      bg = false;
      bd = false;
    //variable qui permet de ne pas redessiner le biome a chaque passage dans la boucle
      firstpassage = true;  
    //Variables remises à zéro pour relancer une partie   
      pointvalidation = 0;
      firstpassagehg = false;
      firstpassagehd = false;
      firstpassagebg = false;
      firstpassagebd = false;
      victoire = false; 
    //Debut du jeu
      tft.fillScreen(ILI9341_WHITE);
      delay(500);
      partiehg();
      x = 25;
      y = 25;
      vy = 0;
      time_begin_function = millis();
      time_begin_game = millis();
      death = false;
}

void plateformes() {
  // si on se trouve sur les plateformes
          if (y >= 40 && y <= 43 && x >= 100 && x < 200){
            plateforme = true;
          } else if (x >= 40 && x < 90 && y >= 90 && y <= 93){
            plateforme = true;
          } else if (x >= 210 && x < 260 && y >= 90 && y <= 93){
            plateforme = true;
          } else if (y >= 140 && y <= 143 && x >= 100 && x < 200){
            plateforme = true;
          } else if (x >= 40 && x < 90 && y >= 190 && y <= 193){
            plateforme = true;
          } else if (x >= 210 && x < 260 && y >= 190 && y <= 193){
            plateforme = true;
          } else {
            plateforme = false;
          }
}

void bord_plateformes() {
  // si on est contre une plateforme
          if(x == 105 && y >43 && y <= 60){
            x = x - 5;
          } else if (x == 200 && y > 40 && y <= 60){
            x = x + 5;
          } else if (x == 45 && y > 90 && y <= 110){
            x = x - 5;
          } else if (x == 90 && y > 90 && y <= 110){
            x = x + 5;
          } else if (x == 215 && y > 90 && y <= 110){
            x = x - 5;
          } else if (x == 260 && y > 90 && y <= 110){
            x = x + 5;
          } else if (x == 105 && y > 140 && y <= 160){
            x = x - 5;
          } else if (x == 200 && y > 140 && y <= 160){
            x = x + 5;
          } else if (x == 45 && y > 190 && y <= 210){
            x = x - 5;
          } else if (x == 90 && y > 190 && y <= 210){
            x = x + 5;
          } else if (x == 215 && y > 190 && y <= 210){
            x = x - 5;
          } else if (x == 260 && y > 190 && y <= 210){
            x = x + 5;
          }
}

void jeu () {
        time_since_function = millis() - time_begin_function; 
        tft.fillRect(x, y, 10, 10, ILI9341_WHITE);  
        deplacement();
        tft.setCursor(200, 15);
        tft.setTextColor(ILI9341_BLACK);
        tft.setTextSize(2);   
        tft.setCursor(220, 10);
        tft.fillRect(220, 10, 30, 20, ILI9341_WHITE);
        tft.print((tempsjeu - (millis() - time_begin_game))/1000);
     
        if((millis() - time_sec_begin) > 1000){ //Refresh du compteur toutes les secondes
           time_sec_begin = millis();
        }

        
    
//--------------------------------------------------------
        // si on se trouve dans la zone haute gauche
        if (hg == true){
          if (x >= 310 && y >= 75 && y <= 175){
            zonehd = true;
            firstpassage = true;
          }
          if(zonehd == true){
            if(firstpassage == true){
              partiehd();
              zonehd = false;
              firstpassage = false;
              hg = false;
              hd = true;
              bg = false;
              bd = false;
              x = 15;
              vy = 0;
            }
          } else if (x >= 100 && x <= 200 && y >= 230){
            firstpassage = true;
            zonebg = true;
            if (zonebg == true){
              if(firstpassage == true){
                partiebg();
                zonebg = false;
                firstpassage = false;
                hg = false;
                hd = false;
                bg = true;
                bd = false;
                y = 15;
                vy = 0;
              }
            }
          }

          plateformes();

          if (firstpassagehg == false){
              if( x == 240 && (y >= 190 && y < 193)){
                pointvalidation ++;
                firstpassagehg = true;
              }
          }

          //si on est sur des zones mortelles
          if ((x < 100 || x > 200) && y >= 227){
            death = true;
          }
          if(x >= 250 && x <= 270 && y >= 187 && y <= 193){
            death = false;
          }
        }
   
//---------------------------------------------------------
        // si on se trouve dans la zone haute droite
        if (hd == true){
          if (x <= 10 && y >= 75 && y <= 175){
            zonehg = true;
            firstpassage = true;
          }
          if(zonehg == true){
            if(firstpassage == true){
              partiehg();
              zonehg = false;
              firstpassage = false;
              hg = true;
              hd = false;
              bg = false;
              bd = false;
              x = 295;
              vy = 0;
            }
          } else if (x >= 100 && x <= 200 && y >= 230){
            firstpassage = true;
            zonebd = true;
            if (zonebd == true){
              if(firstpassage == true){
                partiebd();
                zonebd = false;
                firstpassage = false;
                hg = false;
                hd = false;
                bg = false;
                bd = true;
                y = 15;
                vy = 0;
              }
            }
          }
          
          plateformes();

          if (firstpassagehd == false){
              if( x == 70 && (y >= 190 && y < 193)){
                pointvalidation ++;
                firstpassagehd = true;
              }
          }

          //si on est sur des zones mortelles
          if ((x < 100 || x > 200) && y >= 227){
            death = true;
          }
          if(x >= 40 && x <= 60 && y >= 187 && y <= 193){
            death = false;
          }

        }

//--------------------------------------------------------
        // si on se trouve dans la zone basse gauche
        if (bg == true){
          if (x >= 310 && y >= 75 && y <= 175){
            zonebd = true;
            firstpassage = true;
          }
          if(zonebd == true){
            if(firstpassage == true){
              partiebd();
              zonebd = false;
              firstpassage = false;
              hg = false;
              hd = false;
              bg = false;
              bd = true;
              x = 15;
              vy = 0;
            }
          } else if (x >= 100 && x <= 200 && y <= 10){
            firstpassage = true;
            zonehg = true;
            if (zonehg == true){
              if(firstpassage == true){
                partiehg();
                zonehg = false;
                firstpassage = false;
                hg = true;
                hd = false;
                bg = false;
                bd = false;
                y = 140;
                vy = 0;
              }
            }
          }
          
          plateformes();

          if (firstpassagebg == false){
              if( x == 240 && (y >= 90 && y < 93)){
                pointvalidation ++;
                firstpassagebg = true;
              }
          }

          //si on est sur des zones mortelles
          if (y >= 227){
            death = true;
          }
          if(x >= 250 && x <= 270 && y >= 87 && y <= 93){
            death = false;
          }

        }

//--------------------------------------------------------
        // si on se trouve dans la zone basse droite
        if (bd == true){
          if (x <= 10 && y >= 75 && y <= 175){
            zonebg = true;
            firstpassage = true;
          }
          if(zonebg == true){
            if(firstpassage == true){
              partiebg();
              zonebg = false;
              firstpassage = false;
              hg = false;
              hd = false;
              bg = true;
              bd = false;
              x = 295;
              vy = 0;
            }
          } else if (x >= 100 && x <= 200 && y <= 10){
            firstpassage = true;
            zonehd = true;
            if (zonehd == true){
              if(firstpassage == true){
                partiehd();
                zonehd = false;
                firstpassage = false;
                hg = false;
                hd = true;
                bg = false;
                bd = false;
                y = 140;
                vy = 0;
              }
            }
          }
        
          plateformes();
          

          if (firstpassagebd == false){
              if( x == 70 && (y >= 90 && y < 93)){
                pointvalidation ++;
                firstpassagebd = true;
              }
          }

          // si on est sur des zones mortelles
          if (y >= 227){
            death = true;
          }
          if(x >= 40 && x <= 60 && y >= 87 && y <= 93){
            death = false;
          }
          
        }
      statut();
      delay (10); 
}

