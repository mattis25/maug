#include "SPI.h"
#include "Adafruit_GFX.h"
#include "Adafruit_ILI9341.h"

#define TFT_DC 17
#define TFT_CS 5
#define TFT_MOSI 23
#define TFT_CLK 18
#define TFT_RST 16
#define TFT_MISO 19

//Palette graphique
#define MAUG_BLACK 0x303952
#define MAUG_YELLOW 0xf5cd79
#define MAUG_ROSE 0xc44569

Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC, TFT_MOSI, TFT_CLK, TFT_RST, TFT_MISO);


int LATCH = 35;
int CLOCK = 32;
int DATA = 34;

byte a=0;


void setup() {
  tft.begin();
  tft.fillScreen(MAUG_BLACK);

  pinMode(LATCH, OUTPUT);
  pinMode(CLOCK, OUTPUT);
  pinMode(DATA, INPUT);
  digitalWrite(CLOCK,LOW);
  digitalWrite(LATCH,LOW);

  Serial.begin(115200);

}

void loop() {
  digitalWrite(LATCH,HIGH);

        byte a_temp = shiftInFixed(DATA,CLOCK);

        digitalWrite(LATCH,LOW);

        // Envoie la valeur lue si elle change
        if ( a_temp != a ) {
                a = a_temp;
                Serial.print("A ");
                Serial.println(a,DEC);
        }
}

byte shiftInFixed(byte dataPin, byte clockPin) {
        byte value = 0;

        for (byte i = 0; i < 8; ++i) {
                value = value |  (digitalRead(dataPin) << i);
                digitalWrite(clockPin, HIGH);
                digitalWrite(clockPin, LOW);
        }
        return value;
}
