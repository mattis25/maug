#include "SPI.h"
#include "Adafruit_GFX.h"
#include "Adafruit_ILI9341.h"
#include "WiFi.h"
#include "HTTPClient.h"

// Ecran
#define TFT_DC 17
#define TFT_CS 5
#define TFT_MOSI 23
#define TFT_CLK 18
#define TFT_RST 16
#define TFT_MISO 19
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC, TFT_MOSI, TFT_CLK, TFT_RST, TFT_MISO);

// Boutons tactiles
int threshold = 30;
bool M = false;
bool A = false;
bool U = false;
bool G = false;
bool UP = false;
bool DOWN = false;
bool L = false;
bool R = false;
void gotTouchM(){ M = true; }
void gotTouchA(){ A = true; }
void gotTouchU(){ U = true; }
void gotTouchG(){ G = true; }
void gotTouchUP(){ UP = true; }
void gotTouchDOWN(){ DOWN = true; }
void gotTouchL(){ L = true; }
void gotTouchR(){ R = true; }

char battery_level_pin = 35;
int battery_level; //Battery level, ADC 12 bits

char blue_led = 26;
char white_led = 32;
char red_led = 33;
char motor_pin = 21;
char push_pin = 39;
char spst_pin = 36;

const char* ssid = "iPhone";
const char* password = "internet";

void connectToNetwork(){
  WiFi.begin(ssid, password);

  while(WiFi.status() != WL_CONNECTED){
     delay(1000);
     Serial.println("Establishing connection to WiFi...");
  }

    Serial.println("Connected to network");
    delay(5000);
  
  }


void setup() {
  
  pinMode(battery_level_pin, INPUT);
  pinMode(push_pin, INPUT);
  pinMode(spst_pin, INPUT);
  pinMode(blue_led, OUTPUT);
  pinMode(white_led, OUTPUT);
  pinMode(red_led, OUTPUT);
  pinMode(motor_pin, OUTPUT);

  touchAttachInterrupt(T7, gotTouchM, threshold);
  touchAttachInterrupt(T6, gotTouchA, threshold);
  touchAttachInterrupt(T4, gotTouchU, threshold);
  touchAttachInterrupt(T5, gotTouchG, threshold);
  touchAttachInterrupt(T0, gotTouchUP, threshold);
  touchAttachInterrupt(T3, gotTouchDOWN, threshold);
  touchAttachInterrupt(T2, gotTouchL, threshold);
  touchAttachInterrupt(T1, gotTouchR, threshold);
 
  delay(1000); // Pour laisser le temps d'executer le setup

  Serial.begin(115200);
  //scanNetworks();
  connectToNetwork();
  Serial.println(WiFi.localIP());
  Serial.println(WiFi.macAddress());

  //WiFi.disconnect(true);
  //Serial.println(WiFi.localIP());
  
  
}

void loop() {

  if(WiFi.status() == WL_CONNECTED){
    HTTPClient http;
    http.begin("http://jsonplaceholder.typicode.com/comments?id=10");
    int httpCode = http.GET();

    if(httpCode > 0){
      String payload = http.getString();
      Serial.println(httpCode);
      Serial.println(payload);
      }

      else{
        Serial.println("Error on http request");
        }

        http.end();
  }

  delay(10000);
    }



  
