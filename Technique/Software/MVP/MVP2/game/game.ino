#include "SPI.h"
#include "Adafruit_GFX.h"
#include "Adafruit_ILI9341.h"

// Ecran
#define TFT_DC 17
#define TFT_CS 5
#define TFT_MOSI 23
#define TFT_CLK 18
#define TFT_RST 16
#define TFT_MISO 19
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC, TFT_MOSI, TFT_CLK, TFT_RST, TFT_MISO);

unsigned long time_since_function;
unsigned long time_begin_function;

// Boutons tactiles
int threshold = 30;
bool M = false;
bool A = false;
bool U = false;
bool G = false;
bool UP = false;
bool DOWN = false;
bool L = false;
bool R = false;
void gotTouchM(){ M = true; }
void gotTouchA(){ A = true; }
void gotTouchU(){ U = true; }
void gotTouchG(){ G = true; }
void gotTouchUP(){ UP = true; }
void gotTouchDOWN(){ DOWN = true; }
void gotTouchL(){ L = true; }
void gotTouchR(){ R = true; }

char battery_level_pin = 35;
int battery_level; //Battery level, ADC 12 bits

char blue_led = 26;
char white_led = 32;
char red_led = 33;
char motor_pin = 21;
char push_pin = 39;
char spst_pin = 36;

char backlight_pin = 22; // Not working, le retroéclairage ne peut pas être controllé


// GAME'S VAR
int score = 0;
bool first_game = 1;
bool bonus;
int time_begin_game;
bool in_game = 1;
int y_block;
int y_block2;
bool descente;
bool descente2;
int x;
int y;


void setup() {

  Serial.begin(115200);

  splashscreen(); 

  pinMode(backlight_pin, OUTPUT);  //Retroéclairage écran, USELESS
  digitalWrite(backlight_pin, HIGH);

  pinMode(battery_level_pin, INPUT);
  pinMode(push_pin, INPUT);
  pinMode(spst_pin, INPUT);
  pinMode(blue_led, OUTPUT);
  pinMode(white_led, OUTPUT);
  pinMode(red_led, OUTPUT);
  pinMode(motor_pin, OUTPUT);
  
  touchAttachInterrupt(T7, gotTouchM, threshold);
  touchAttachInterrupt(T6, gotTouchA, threshold);
  touchAttachInterrupt(T4, gotTouchU, threshold);
  touchAttachInterrupt(T5, gotTouchG, threshold);
  touchAttachInterrupt(T0, gotTouchUP, threshold);
  touchAttachInterrupt(T3, gotTouchDOWN, threshold);
  touchAttachInterrupt(T2, gotTouchL, threshold);
  touchAttachInterrupt(T1, gotTouchR, threshold);
 
  delay(1000); // Pour laisser le temps d'executer le setup
  
}

void loop() {
  
  game();

}

void splashscreen(){
  tft.begin();
  tft.setRotation(3);
  delay(1000);
   
  tft.fillScreen(ILI9341_WHITE);
  tft.setCursor(92, 90);
  tft.setTextColor(ILI9341_BLACK); tft.setTextSize(5);
  tft.println("Maug");
  delay(1200);
  tft.setCursor(50, 200);
  tft.setTextSize(2);
  tft.println("Appuyez sur A pour");
  tft.setCursor(90, 220);
  tft.print("continuer");  
}


void game(){
  
  if(A){
    
    if(first_game == 1){      // Les règles sont affichées à chaque démarrage de la console mais pas à chaque partie
      tft.fillScreen(ILI9341_WHITE);
      tft.setCursor(20, 20);
      tft.setTextColor(ILI9341_BLACK);
      tft.setTextSize(3);
      tft.print("Regles du jeu");
      tft.setCursor(15, 65);
      tft.setTextSize(2);
      tft.print("Atteindre la zone verte");
      tft.setCursor(15, 90);
      tft.print("Eviter les zones rouges ");
      //tft.setCursor(15, 107);
      //tft.print("zones rouges");
      tft.setCursor(15, 115);
      tft.print("Deplacements: M A U G");
      tft.setCursor(15, 140);
      tft.print("En respectant le chrono!"); 
      tft.setCursor(15, 165);
      tft.print("Bonus: disque jaune");
      tft.setCursor(15, 210);
      delay(1000);
      A = 0;
      tft.print("A pour commencer le jeu");
      first_game = 0;        
      }

      if(A){
      // On redonne les valeurs par défaut aux variables
      digitalWrite(blue_led, LOW);
      digitalWrite(red_led, LOW);
      A = 0;
      in_game = 1;
      y_block = 0;
      y_block2 = 195;
      x = 10;
      y = 75;
      bonus = 0;
    
      tft.fillScreen(ILI9341_WHITE);
      delay(600);
      time_begin_function = millis();

      //On construit les blocs fixes
      tft.fillRect(0,0,100,60, ILI9341_RED);
      tft.fillRect(50,55,50,60, ILI9341_RED);
      tft.fillRect(160,0,60,196, ILI9341_RED);
      tft.fillRect(265,210, 70, 40, ILI9341_GREEN);
      tft.fillCircle(155, 20, 5, ILI9341_YELLOW); 

      delay(500);
      
      time_begin_game = millis();
      int time_sec_begin = millis();
      
   while(in_game == 1){    
      time_since_function = millis() - time_begin_function; 
      moving_block();
      tft.setCursor(200, 15);
      tft.setTextColor(ILI9341_BLACK);
      tft.setTextSize(2);
      tft.print("Score:");
      tft.print(score);   
      tft.setCursor(20, 190);
      tft.print("Temps restant");
      tft.setCursor(20, 210);
      tft.print((8000 - (millis() - time_begin_game))/1000);
      tft.print(" secondes");
     
   if((millis() - time_sec_begin) > 1000){ //Refresh du compteur toutes les secondes (pour éviter un clignotement permanent si c'était à chaque passage dans la boucle)
      time_sec_begin = millis();
      tft.fillRect(20, 210, 20, 20, ILI9341_WHITE);
   }

   if((millis() - time_begin_game) > 8000){ //Limite de temps (8 secondes)
      in_game = 0;
      tft.setTextSize(4);
      tft.setCursor(100, 85);
      tft.setTextColor(ILI9341_BLACK);
      tft.print("Temps");
      tft.setCursor(90, 130);
      tft.print("ecoule");
      delay(3000);
      losing();
   }
      
   if(time_since_function > 110 && in_game == 1){ //Déplacement du carré bleu
      time_begin_function = millis();             //On utilise pas la fonction delay() car elle arrète tout le script
      tft.fillRect(x, y, 10, 10, ILI9341_WHITE);  

      if(M && y > 20){
        y = y - 20; 
      }

      if(U && y < 201){
        y = y + 20;
      }

      if(G && x > 20){
        x = x - 20; 
      }

      if(A && x < 290){
        x = x + 20;
      }

      M = 0;
      A = 0;
      U = 0;
      G = 0;
      tft.fillRect(x, y, 10, 10, ILI9341_BLUE);
      /*tft.setCursor(20, 190);     //DEBUG (donne les coordonnées du carré bleu) -- Chevauchement avec le chronomètre
      tft.setTextColor(ILI9341_BLACK);
      tft.setTextSize(2);
      tft.print(x);
      tft.setCursor(20, 215);
      tft.setTextColor(ILI9341_BLACK);
      tft.setTextSize(2);
      tft.print(y);/*
      tft.setCursor(40, 190);
      tft.print(y_block2);
      tft.setCursor(40, 215);
      tft.print(descente);     */

      if(x == 150 && y == 15){ //Si le carré passe sur le jeton bonus 
        bonus = 1;
        Serial.println("Bonus");
        Serial.print(bonus);
      }

      //Si le carré passe sur une jaune rouge
      if((x < 101 && y<60) || (x > 49 && x < 101 && y < 115) || (x > 159 && x < 221 && (y > (y_block - 10) && y < (y_block2) ) )  ){ //|| y < (y_block2 + 10) 
        in_game = 0;
        losing();   
      }

      //Si le carré arrive dans la zone verte
      if(x > 250 && y > 195){
         in_game = 0;
         winning();   
      }
    }
    }  
  }
  } 
  }


void losing(){
  digitalWrite(motor_pin, HIGH);
  digitalWrite(red_led, HIGH);
  tft.fillScreen(ILI9341_RED);
  tft.setCursor(40, 90);
  tft.setTextSize(4);
  tft.setTextColor(ILI9341_WHITE);
  tft.print("Perdu");
  tft.setCursor(75, 160);
  delay(1500);
  digitalWrite(motor_pin, LOW);
  A = 0;
  tft.setTextSize(2);
  tft.print("Appuyez sur A");
  tft.setCursor(72, 185);
  tft.print("pour reessayer");
  score--;
}


  void winning(){ 
  digitalWrite(white_led, HIGH); //Ne fonctionne pas
  score++;
  tft.fillScreen(ILI9341_GREEN);
  tft.setCursor(40, 70);
  tft.setTextSize(4);
  tft.setTextColor(ILI9341_WHITE);
  tft.print("Gagne!");
  Serial.println("gagné");
  Serial.print(bonus);
    
  if(bonus == 1){
    tft.setTextSize(2);
    tft.setCursor(60, 120);
    tft.print("Avec le bonus!");
  }

  if(bonus == 0){
    tft.setTextSize(2);
    tft.setCursor(60, 120);
    tft.print("Sans le bonus...");
  }
    
  delay(1500);
  A = 0;
  tft.setCursor(75, 180);
  tft.setTextSize(2);
  tft.print("Appuyey sur A");
  tft.setCursor(80, 205);
  tft.print("pour rejouer");
    
}


void moving_block(){

//Le bloc qui se déplace est enfait composé d'une partie fixe (celle qui est tout le temps coloré) et de deux parties (aux extrémités)
//Les parties aux extrémités "grandissent" et "rapetissent" en suivant un cycle, ce qui donne l'impression que tout le bloc se déplace
    
  if(y_block == 0){ 
    descente = 0;
  }

  if(y_block == 45){
    descente = 1;
  }

  if(descente == 1){
     y_block--;
     tft.drawLine(160, y_block, 219, y_block, ILI9341_RED);
  }  

  if(descente == 0){
     tft.drawLine(160, y_block, 219, y_block, ILI9341_WHITE);
     y_block++;
  }

  if(y_block2 == 195){
     descente2 = 0;
  }

  if(y_block2 == 240){
    descente2 = 1;
  }

  if(descente2 == 1){
     tft.drawLine(160, y_block2, 219, y_block2, ILI9341_WHITE);
     y_block2--;
  }  

  if(descente2 == 0){  
     y_block2++;
     tft.drawLine(160, y_block2, 219, y_block2, ILI9341_RED);
  }

  delay(25); // Influe sur la vitesse de déplacement des blocs
    
}
