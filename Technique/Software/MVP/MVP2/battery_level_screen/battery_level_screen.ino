#include "SPI.h"
#include "Adafruit_GFX.h"
#include "Adafruit_ILI9341.h"

#define TFT_DC 17
#define TFT_CS 5
#define TFT_MOSI 23
#define TFT_CLK 18
#define TFT_RST 16
#define TFT_MISO 19

//Palette graphique
#define MAUG_BLACK 0x303952
#define MAUG_YELLOW 0xf5cd79
#define MAUG_ROSE 0xc44569

Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC, TFT_MOSI, TFT_CLK, TFT_RST, TFT_MISO);// For the Adafruit shield, these are the default.


void setup() {
 tft.begin();
 tft.print("Affichage du niveau de batterie");
 Serial.begin(115200);
Serial.println("Batterie");
pinMode(35, INPUT);

}

//3.9V: 2119
//4.2V: 2335
  
void loop() {
  int battery = analogRead(35);
  Serial.println(battery);
  tft.fillScreen(ILI9341_BLACK);
  tft.setTextSize(2);
  tft.setTextColor(ILI9341_WHITE);
  tft.setCursor(20,30);
  tft.print(battery);
  delay(5000);


}
