// Lit si on touche un des disques (donne une valeur et pas "Touché" "Non touché"

void setup()
{
  Serial.begin(115200);
  delay(1000); // give me time to bring up serial monitor
  Serial.println("ESP32 Touch Test");
}

void loop()
{
  Serial.print("M "); //M
  Serial.println(touchRead(T7));
  
   Serial.print("A ");
  Serial.println(touchRead(T6)); 
  
  Serial.print("U ");
  Serial.println(touchRead(T4)); 
 
  Serial.print("G ");
  Serial.println(touchRead(T5)); 

    Serial.print("Haut: "); 
  Serial.println(touchRead(T0));
  
   Serial.print("Bas: ");
  Serial.println(touchRead(T3));
  
  Serial.print("Gauche: ");
  Serial.println(touchRead(T2));
 
  Serial.print("Droite: ");
  Serial.println(touchRead(T1));
  
  Serial.println("");
  Serial.println("");
  delay(3000);
}
