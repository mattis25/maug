#include "WiFi.h"                

#include "bitmaps.h"
#include "bitmapsLarge.h"

#include "SPI.h"
#include "Adafruit_GFX.h"
#include "Adafruit_ILI9341.h"
#define TFT_DC 17
#define TFT_CS 5
#define TFT_MOSI 23
#define TFT_CLK 18
#define TFT_RST 16
#define TFT_MISO 19
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC, TFT_MOSI, TFT_CLK, TFT_RST, TFT_MISO);

unsigned long time_since_function;
unsigned long time_begin_function;

uint16_t x0 = 15;
uint16_t y = 40;

int n;

int threshold = 30;
bool M = false;
bool A = false;
bool U = false;
bool G = false;
void gotTouchM(){ M = true; }
void gotTouchA(){ A = true; }
void gotTouchU(){ U = true; }
void gotTouchG(){ G = true; }

int step = 0;


void setup() {

  pinMode(22, OUTPUT);  //Retroéclairage écran
  digitalWrite(22, HIGH);

  
  splashscreen(); 
  
  pinMode(35, INPUT);  //Haut
  pinMode(32, INPUT);   //Gauche
  pinMode(33, INPUT);   //Bas
  pinMode(36, INPUT);  // Droite
  pinMode(39, INPUT);   //Interrupteur
  pinMode(34, INPUT);   //Maug +
  touchAttachInterrupt(T7, gotTouchM, threshold);
  touchAttachInterrupt(T6, gotTouchA, threshold);
  touchAttachInterrupt(T4, gotTouchU, threshold);
  touchAttachInterrupt(T5, gotTouchG, threshold);
  
  Serial.begin(115200);

 WiFi.mode(WIFI_STA);
 WiFi.disconnect();
 delay(1000);
 
}

void loop() {



delay(300);

    menu();
    wifiScan(); 

}

void splashscreen(){
   tft.begin();
   tft.setRotation(3);
   
  delay(1000);
   
   tft.fillScreen(ILI9341_WHITE);
   tft.setCursor(92, 90);
   tft.setTextColor(ILI9341_BLACK); tft.setTextSize(5);
   tft.println("Maug");
   delay(1200);

   tft.setCursor(50, 200);
   tft.setTextSize(2);
   tft.println("Appuyez sur A pour");
   tft.setCursor(90, 220);
   tft.print("continuer");

   
  }

void menu(){
  if(A){
    M = false;
     G = false;
    time_begin_function = millis();
    tft.fillScreen(ILI9341_WHITE);
    tft.setCursor(30, 20);
    tft.setTextSize(3); tft.setTextColor(ILI9341_BLACK);
    tft.println("Menu"); 

    tft.drawTriangle(30, 90, 30, 150, 80, 120, ILI9341_BLACK); 
    tft.setCursor(30, 155);
    tft.setTextSize(2);
    tft.println("Play");
    tft.drawBitmap(160,100, bell,16,16,ILI9341_BLACK);
    
     do{
      wifiScan();
       playGame();
        time_since_function = millis() - time_begin_function; 
        delay(10);
        Serial.println(time_since_function);
        
      } while (time_since_function < 30000);
  }
}



void playGame() {

  if(M){
tft.fillScreen(ILI9341_WHITE);    
    
    }
  
  }

void wifiScan(){

  if(G){
    A = false;
    M = false;
    
    tft.fillScreen(ILI9341_WHITE);
    tft.setCursor(25, 100);
    tft.setTextSize(2); tft.setTextColor(ILI9341_BLACK);
    tft.println("Chargement en cours"); 
    

    tft.setCursor(10,10);
    tft.setTextSize(2); tft.setTextColor(ILI9341_BLACK);

     n = WiFi.scanNetworks();
  
    tft.fillScreen(ILI9341_WHITE);
    tft.setCursor(5, 5);

    time_begin_function = millis();
    
    if (n == 0) {
      tft.println("Aucun reseau trouve");
    }
    
    else {
      tft.print(n);

      if(n == 1){
      tft.println(" reseau wifi detecte:");
      }

       else{
      tft.println(" reseaux wifi detectes:");
      }
      
      for (int i = 0; i < n; ++i) {
        tft.setTextSize(2); tft.setCursor(20, (i*20 + 40)); 
        //.remove(20)
        String wifiSSID = WiFi.SSID(i);
        wifiSSID.remove(22);  //Limite la longueur du nom à 22 caractères
        tft.print(wifiSSID);
        delay(10);
       }

         tft.drawFastVLine(x0 , y, 16, ILI9341_BLACK);
    }
    
    tft.println("");
    delay(300);
  
  


    

    do{
        menu();
        time_since_function = millis() - time_begin_function; 
        delay(10);
        Serial.println(time_since_function);
        
   
          if(digitalRead(33) == 1 && y < (n*20 + 20)){   //&& y < (n*20 + 20)
    tft.drawFastVLine(x0, y, 16, ILI9341_WHITE);
    y = y + 20 ;
    tft.drawFastVLine(x0 , y, 16, ILI9341_BLACK);
    delay(300);
  }

  if(digitalRead(35) == 1 && y > 40){   //&& y > 40
    tft.drawFastVLine(x0, y, 16, ILI9341_WHITE);
    y = y - 20 ;
    tft.drawFastVLine(x0 , y, 16, ILI9341_BLACK);
    delay(300);
  }
        
      } while (time_since_function < 30000);
}
}
  

